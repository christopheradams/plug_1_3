## {{ site.title }}

Changes for Elixir web apps with Plug 1.3

[christopheradams.io](https://christopheradams.io)

---

## Elixir Web Apps

**[Plug]** is a specification and library for composable modules in between web applications

**[Phoenix]** is a complete web framework built using **Plug**, which adds
views, templates, web sockets, database integration, etc.

[Plug]: https://github.com/elixir-lang/plug
[Phoenix]: http://www.phoenixframework.org/

---

## The Plug Stack

1. **[`Plug`]()** specification
1. `Plug.`**[`Conn`]()** {% fragment %}
1. `Plug.`**[`Builder`]()** {% fragment %}
1. `Plug.`**[`Router`]()** {% fragment %}

---

## `Plug.`**[`Conn`]()**

*A data structure which describes a web request and response*

```elixir
conn = %Plug.Conn{
  host: "www.example.com",
  method: "GET",
  request_path: "/hello"
  # etc...
}
```

---

## **[`Plug`]()** Specification

1. **[Function]()** plugs
1. **[Module]()** plugs

--

## **[Function]()** plugs

Type signature:

```elixir
(Plug.Conn.t, Plug.opts) :: Plug.Conn.t
```

----

Example:

```elixir
def json_header_plug(conn, _opts) do
  Plug.Conn.put_resp_content_type(conn, "application/json")
end
```

--

## **[Module]()** plugs (1)

Callbacks:

```elixir
@callback init(opts) :: opts
@callback call(Plug.Conn.t, opts) :: Plug.Conn.t
```

--

## **[Module]()** plugs (2)

Example:

```elixir
defmodule JSONHeaderPlug do
  import Plug.Conn

  def init(opts) do
    opts
  end

  def call(conn, _opts) do
    put_resp_content_type(conn, "application/json")
  end
end
```

*Sets a header*

Usage:

```elixir
conn = %Plug.Conn{}
JSONHeaderPlug.call(conn, JSONHeaderPlug.init([]))
```

--

## **[Module]()** plugs (3)

Example:

```elixir
defmodule MyApp.HelloPlug do
  import Plug.Conn

  def init(opts) do
    opts
  end

  def call(conn, _opts) do
    send_resp(conn, 200, "hello")
  end
end
```

*Returns a response*

Usage:

```elixir
conn = %Plug.Conn{}
MyApp.HelloPlug.call(conn, MyApp.HelloPlug.init([]))
```

---

## `Plug.`**[`Builder`]()**

*Used to build plug pipelines*<br/>
Defines a **[`plug`]()** macro that compiles all the plugs into a stack

Example:

```elixir
defmodule MyApp do
  use Plug.Builder

  plug Plug.Logger
  plug :hello, upper: true

  def hello(conn, opts) do
    body = if opts[:upper], do: "WORLD", else: "world"
    send_resp(conn, 200, body)
  end
end
```

*This is how Plug is "composable"*

---

## `Plug.`**[`Router`]()**

*A domain-specific-language (DSL) for request routing*

- Defines **[`match`]()** and **[`dispatch`]()** plugs
- Adds macros that match a URL path with a function body

Example:

```elixir
defmodule MyApp.AppRouter do
  use Plug.Router

  plug :match
  plug :dispatch

  get "/hello" do
    send_resp(conn, 200, "world")
  end

  match _ do
    send_resp(conn, 404, "oops")
  end
end
```

*Remind you of Sinatra?*

---

`Plug.Conn.send_resp(conn, 200, "THE END")`

*That was the story of Plug*

---

## The Phoenix Stack

1. `Phoenix.`**[`Endpoint`]()** {% fragment %}
1. `Phoenix.`**[`Router`]()** {% fragment %}
1. `Phoenix.`**[`Controller`]()** {% fragment %}

*all plugs* {% fragment %}

---

## `Phoenix.`**[`Router`]()**

*Adds macros that match a URL path with a contoller*

```elixir
defmodule MyApp.Router do
  use Phoenix.Router

  get "/hello", MyApp.HelloController, :show
end
```

---

## `Phoenix.`**[`Controller`]()**

*A module to organize resource functions*

```elixir
defmodule MyApp.HelloController do
  use MyApp.Web, :controller

  def show(conn, _params) do
    send_resp(conn, 200, "hello")
  end
end
```

*but since Controllers are Plugs...*

---

## `Phoenix.`**[`Router`]()**

*Phoenix match macros will accept **any** Plug*

```elixir
defmodule MyApp.Router do
  use Phoenix.Router

  get "/hello", MyApp.HelloController, :show
  get "/world", MyApp.HelloPlug, []
end
```

*We couldn't do this in plain Plug, until now...*

---

## New In [Plug 1.3]()

```elixir
defmodule MyApp.Router do
  use Plug.Router

  plug :match
  plug :dispatch

  get "/hello" do
    send_resp(conn, 200, "world")
  end

  get "/world", to: MyApp.HelloPlug, init_opts: []

  match "/any", to: MyApp.SomePlug, init_opts: :any
end
```

*Syntax is just a little different from Phoenix*

*Now you can write Controllers for Plug, and more...*

---

## Match Any [Plug]() vs. [Phoenix]()

```elixir
# Plug
defmodule MyApp.Router do
  use Plug.Router

  plug :match
  plug :dispatch

  match "/any", to: MyApp.SomePlug, init_opts: :any
end
```

```elixir
# Phoenix
defmodule MyApp.Router do
  use Phoenix.Router

  match :*, "/any", MyApp.SomePlug, :any
end
```

*Use this if the HTTP verb handling should happen in the Controller instead of
the Router*

*(This was not documented in Phoenix < 1.3)*

---

## [`PlugRest`]().**`Router`**

*REST behaviour and router for hypermedia web apps*

```elixir
defmodule MyRouter do
  use PlugRest.Router

  plug :match
  plug :dispatch

  # new resource macro
  resource "/hello", MyApp.HelloResource, "World"

  # does the same thing as
  match "/hello_plug", to: MyApp.HelloResource, init_opts: "World"
end
```

[github.com/christopheradams/plug_rest](https://github.com/christopheradams/plug_rest)

---

## [`PlugRest`]().**`Resource`**

```elixir
defmodule MyApp.HelloResource do
  use PlugRest.Resource

  def allowed_methods(conn, state) do
    {["HEAD", "GET", "OPTIONS"], conn, state}
  end

  def content_types_provided(conn, state) do
    {[{"text/html", :to_html}], conn, state}
  end

  def to_html(conn, state) do
    {"Hello #{state}", conn, state}
  end
end
```

*Inspired by Webmachine and cowboy_rest*

*Handles HTTP responses like 405 and OPTIONS*


---

## Elixir Style Guide


[github.com/christopheradams/elixir_style_guide](https://github.com/christopheradams/elixir_style_guide)


---

The End
